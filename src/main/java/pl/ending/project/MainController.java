package pl.ending.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.ending.project.users.EventService;

@Controller
public class MainController {

    private EventService eventService;

    @Autowired
    public MainController(EventService eventService) {
        this.eventService = eventService;
    }

    @RequestMapping("/")
    public String home(Model model){
        model.addAttribute("eventsList", eventService.findAllEventAfterNow());
        return "index";
    }
}
