package pl.ending.project.users;

public class UserExistsException extends RuntimeException {

    public UserExistsException(String message){
        super(message) ;
    }

}
