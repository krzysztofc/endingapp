package pl.ending.project.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class EventController {


    private EventService eventRegistrationService;

    @Autowired
    public EventController(EventService eventRegistrationService) {
        this.eventRegistrationService = eventRegistrationService;
    }

    @GetMapping("/event")
    public String event(Model model) {
        model.addAttribute("eventRegistrationDTO", new EventRegistrationDTO());
        return "event";
    }

    @PostMapping("/event")
    public String createEvent(@ModelAttribute @Valid EventRegistrationDTO eventRegistrationDTO, BindingResult validationResult) {
        if (validationResult.hasErrors()) {
            return "event";
        }
        eventRegistrationService.registerNewEvent(eventRegistrationDTO);
        return "event";
    }
}
