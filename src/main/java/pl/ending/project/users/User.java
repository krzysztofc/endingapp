package pl.ending.project.users;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@Entity
@ToString(exclude = {"password"})
public class User {

    @Id
    @GeneratedValue
    private Integer id;
    @Column(unique = true)
    private String email;
    @Column(length = 50)
    private String displayName;
    @Column(length = 500)
    private String password;
    @ManyToMany
    @JoinTable(name = "user_role")
    private Set<Role> userRole;

}
