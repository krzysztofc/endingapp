package pl.ending.project.users;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.stereotype.Service;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

@Service
@Getter
@Setter
@ToString(exclude = {"password"})
public class UserRegistrationDTO {

    @Size(min=3, max = 20, message = "Wyświetlana nazwa powinna zawierac od {min} do {max}, podałeś ${validatedValue}.")
    private String displayName;
    @Email(message = "Podałeś nieprawidłowy email.")
    private String email;
    @Size(min=3, max = 20, message = "Hasło powinno zawierać od {min} do {max}.")
    private String password;

}
