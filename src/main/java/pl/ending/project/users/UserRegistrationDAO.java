package pl.ending.project.users;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRegistrationDAO extends JpaRepository<User, Integer> {

    User findUserByEmail(String email); //todo chanege to Optional<User>
}
