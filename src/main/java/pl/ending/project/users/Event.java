package pl.ending.project.users;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Event {


    @Id
    @GeneratedValue
    private Integer id;
    @NotBlank
    private String title;
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Size(min = 20)
    private String description;
    @ManyToOne
    private User user;

}
