package pl.ending.project.users;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserRegistrationService {


    private PasswordEncoder passwordEncoder;
    private UserRegistrationDAO userRegistrationDAO;
    private RoleRepository roleRepository;

    @Autowired
    public UserRegistrationService(PasswordEncoder passwordEncoder, UserRegistrationDAO userRegistrationDAO, RoleRepository roleRepository) {
        this.passwordEncoder = passwordEncoder;
        this.userRegistrationDAO = userRegistrationDAO;
        this.roleRepository = roleRepository;
    }

    public void registerUser(UserRegistrationDTO userRegistrationDTO) throws UserExistsException {
        if (userExists(userRegistrationDTO.getEmail())) {
            throw new UserExistsException("Użytkownik o emailu " + userRegistrationDTO.getEmail() + " istnieje!");
        }
        User user = rewriteUserRegistrationDTOToUserWithDefaultRole(userRegistrationDTO);
        userRegistrationDAO.save(user);
    }

    private User rewriteUserRegistrationDTOToUserWithDefaultRole(UserRegistrationDTO userRegistrationDTO) {
        User user = new User();
        user.setEmail(userRegistrationDTO.getEmail());
        user.setDisplayName(userRegistrationDTO.getDisplayName());
        user.setPassword(passwordEncoder.encode(userRegistrationDTO.getPassword()));

        Set<Role> userRoles = new HashSet<>();
        Role role = roleRepository.findRoleByRoleName(Role.ROLE_USER); // stala ROLE_USER zadeklarowana w Role
        if (role == null) {
            role = roleRepository.save(new Role(Role.ROLE_USER));
        }
        userRoles.add(role);
        user.setUserRole(userRoles);
        return user;
    }

    private boolean userExists(String email) {
        if (userRegistrationDAO.findUserByEmail(email) != null) {
            return true;
        }
        return false;
    }
}
