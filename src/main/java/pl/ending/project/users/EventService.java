package pl.ending.project.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class EventService {

    private EventRepository eventRepository;
    private UserContextHolder userContextHolder;
    private UserRegistrationDAO userRegistrationDAO;

    @Autowired
    public EventService(EventRepository eventRepository, UserContextHolder userContextHolder, UserRegistrationDAO userRegistrationDAO) {
        this.eventRepository = eventRepository;
        this.userContextHolder = userContextHolder;
        this.userRegistrationDAO = userRegistrationDAO;
    }

    public void registerNewEvent(EventRegistrationDTO eventRegistrationDTO) {
        Event event = rewriteEventRegistrationDTOToEvent(eventRegistrationDTO);
        eventRepository.save(event);
    }

    private Event rewriteEventRegistrationDTOToEvent(EventRegistrationDTO eventRegistrationDTO) {
        Event event = new Event();
        String userLoggedIn = userContextHolder.getUserLoggedIn();
        User user = userRegistrationDAO.findUserByEmail(userLoggedIn);

        event.setTitle(eventRegistrationDTO.getTitle());
        event.setDescription(eventRegistrationDTO.getDescription());
        event.setDate(eventRegistrationDTO.getDate());
        event.setUser(user);
        return event;
    }

    public List<Event> findAllEvents() {
        return eventRepository.findAll();
    }

    public List<Event> findAllEventAfterNow() {
        Date date = new Date();
        return eventRepository.findAllByDateAfter(date);
    }
}
