package pl.ending.project.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class UserController {


    private UserRegistrationService userRegistrationService;

    @Autowired
    public UserController(UserRegistrationService userRegistrationService) {
        this.userRegistrationService = userRegistrationService;
    }

    @GetMapping("/registerForm")
    public String register(Model model) {
        model.addAttribute("userRegistrationDTO", new UserRegistrationDTO());
        return "registerForm";
    }

    @PostMapping("/registerForm")
    public String registerEffect(@ModelAttribute @Valid UserRegistrationDTO userRegistrationDTO, BindingResult validationResult, Model model) {
        if (validationResult.hasErrors()) {
            return "registerForm";
        }
        try {
            userRegistrationService.registerUser(userRegistrationDTO);
        } catch (UserExistsException e) {
            validationResult.rejectValue("email","user.exists","Użytkownik już istnieje");
            return "registerForm";
        }
        model.addAttribute("userEmail", userRegistrationDTO.getEmail());
        return "redirect:/";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }
}
